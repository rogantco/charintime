const md5 = require('md5');
const ts = 'mysalt';
const privateKey = 'ac070de52f0f8da50ca2ad7cad0540f5b6a9ca09';
const publicKey = 'b94dc2dd8e34a06d2c7c5079b534d18b';
const baseApiUrl = 'https://gateway.marvel.com/v1/public';
const hash = md5(ts+privateKey+publicKey);

module.exports = {
    baseApiUrl,
    hash,
    publicKey,
    ts,
};
