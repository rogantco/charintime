const fetch = require('node-fetch');
const { baseApiUrl, ts, publicKey, hash } = require('./index');

const characterType = `
    type Character {
        id: ID
        name: String
        description: String
    }
`;

const characterQuery = `
    getCharacters(name: String): [Character]
`;

const getCharacters = function ({name}) {
    return fetch(`${baseApiUrl}/characters?nameStartsWith=${name}&ts=${ts}&apikey=${publicKey}&hash=${hash}`)
        .then(res => res.json())
        .then(json => {
            return json.data.results.map((character => {
                return {
                    id: character.id,
                    name: character.name,
                    description: character.description,
                }
            }))
        });
}

module.exports = {
    characterQuery,
    characterType,
    getCharacters,
};
