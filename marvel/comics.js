const fetch = require('node-fetch');
const { baseApiUrl, ts, publicKey, hash } = require('./index');

const comicType = `
    type Comic {
        id: ID
        description: String
        isbn: String
        title: String
        pages: String
        cover: String
    }
`;

const comicQuery = `
    getComics(character: Int): [Comic]
`;

const getComics = function ({character}) {
    return new Promise((resolve, reject) => {
        fetch(`${baseApiUrl}/characters/${character}/comics?ts=${ts}&apikey=${publicKey}&hash=${hash}`)
        .then(res => res.json())
        .then(json => {
            const comics = json.data.results.map((comic => {
                return {
                    id: comic.id,
                    description: comic.description,
                    isbn: comic.isbn,
                    title: comic.title,
                }
            })).filter(comic => comic.isbn !== '');

            return getAditionalInfo(comics, resolve);
        });
    })
}

const getAditionalInfo = function(comics, resolve) {
    const ISBNs = comics.map(comic => 'ISBN:' + comic.isbn).join();

    fetch(`https://openlibrary.org/api/books?bibkeys=${ISBNs}&format=json&jscmd=data`)
        .then(res => res.json())
        .then(results => {
            resolve(Object.keys(results).map((isbn) => { 
                const isbnComic = results[isbn];
                const comic = comics.find(comic => comic.isbn === isbn.replace('ISBN:', '') )

                if (comic) {
                    return {
                        ...comic,
                        pages: isbnComic.number_of_pages,
                        cover: isbnComic.cover.large,
                    }
                }
            }))
        });

}

module.exports = {
    comicQuery,
    comicType,
    getComics,
};
