const express = require('express');
const graphqlHTTP = require('express-graphql');
const { buildSchema } = require('graphql');
const { getCharacters, characterQuery, characterType } = require('./marvel/characters');
const { getComics, comicQuery, comicType } = require('./marvel/comics');

const app = express();


app.use('/graphql', graphqlHTTP({
  schema: buildSchema(`
    ${characterType}
    ${comicType}

    type Query {
        ${characterQuery}
        ${comicQuery}
    }
  `),
  rootValue: {
    getCharacters,
    getComics,
  },
  graphiql: true
}));

app.listen(4000);
